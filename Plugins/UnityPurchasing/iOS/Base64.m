#import "Base64.h"


@implementation NSData (mgb64_NSDataBase64Encoding)

- (NSString *)mgb64_base64EncodedString
{
    return [self base64EncodedStringWithOptions:0];
}

@end
