#import <Foundation/Foundation.h>


NS_ASSUME_NONNULL_BEGIN

@interface NSData (mgb64_NSDataBase64Encoding)

- (NSString *)mgb64_base64EncodedString;

@end

NS_ASSUME_NONNULL_END
